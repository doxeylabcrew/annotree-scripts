

Domain Search funcionality:

- User inputs a list of Pfam Ids
- returns 
```
[tree_node_ids ... ] list of numbers
```
- this data will be used subsequently in `QueryActionHandler` to highlight appropriate nodes

Node detail container:

- remove any existing architecture information
- keep the green bar that shows hits and misses when clicking on an ancestral node

