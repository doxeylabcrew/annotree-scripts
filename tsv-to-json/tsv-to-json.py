#!/usr/bin/python3

import argparse
from Bio import Phylo
import json
import re

taxonomy_dict = {}

i = 1


def _idUp(up):
    global i
    _i = i
    if up:
        i += 1
    else:
        i -= 1
    return _i


def get_taxonomy_levels(genome_name):
    global taxonomy_dict
    return taxonomy_dict[genome_name]


def clade_to_dict(clade, parentId=0):
    name = clade.name or ''
    level = name
    level_match = re.compile('__([^;]+);?').search(name)
    if level_match:
        level = level_match.group(1)
    branch_length = max(float(clade.branch_length or 0), 0.00001)
    my_id = _idUp(True)
    children = [clade_to_dict(c, parentId=my_id) for c in clade.clades]
    children = [x for x in children if x is not None]
    isLeaf = len(children) == 0

    bootstrap_match = re.compile('^([0-9.]+):').search(name)
    bootstrap = None
    if bootstrap_match:
        bootstrap = bootstrap_match.group(1)
    rank = 'unranked'
    ranks = []  # tracks all possible ranks a node can be, e.g. it can be both a phylum and a class
    if ':' in name: # only true for internal nodes
        d = {
            'd': 'domain',
            'p': 'phylum',
            'c': 'class',
            'o': 'order',
            'f': 'family',
            'g': 'genus',
            # 's': 'species' species will no longer appear in internal nodes
        }
        # must be an internal node
        rank_and_names = name.split(':')[1].split('; ')
        for r in rank_and_names:
            temp_rank = d[r.split('__')[0]]
            temp_rank_name = r.split('__')[1]
            ranks.append({'rank': temp_rank, 'rank_name': temp_rank_name})
        # rank is the first in ranks, the highest one
        rank = ranks[0]['rank']
    taxonomy_levels = {}
    if isLeaf:
        try:
            taxonomy_levels = get_taxonomy_levels(level)
        except KeyError:
            _idUp(False)
            print('KeyError: Genome \'{}\' is in the tree but not the taxonomy. It will be excluded from the JSON tree'.format(level))
            return(None)
        # since GTDB version 89, released in 2019 Aug, each genome is also
        # the representative for its species
        # previous extraction method assumes species is in internal nodes
        # we have to look up species info in taxonomy_levels for leaves
        ranks.append({'rank': 'species', 'rank_name': taxonomy_levels.get('species','')})
        rank = 'species'
    taxId = -1
    d = {
        'bootstrap': bootstrap,
        'length': branch_length,
        'children': children,
        'isLeaf': isLeaf,
        'rank': rank,
        'id': my_id,
        'parentId': parentId,
        'taxId': taxId,
        'genusId': 123,
        'level': level,
        'counts': 0,
        'ranks': ranks,
        'taxonomy_levels': taxonomy_levels,
    }
    return d


def parseNewick(fileName):
    tree = Phylo.read(fileName, 'newick')
    return clade_to_dict(tree.clade)


def _parse_tax_entry(entry):
    columns = entry.strip().split('\t')


# sample line of tsv:
# GB_GCA_003309125.1      d__Bacteria;p__Firmicutes;c__Bacilli;o__Staphylococcales;f__Staphylococcaceae;g__Staphylococcus;s__Staphylococcus aureus
# the spaces are actually a tab
def parse_taxonomy(fileName):
    global taxonomy_dict
    taxonomy_dict = {}
    d = {
        'd': 'domain',
        'p': 'phylum',
        'c': 'class',
        'o': 'order',
        'f': 'family',
        'g': 'genus',
        's': 'species'
    }
    # parsing valid as of Aug 2019
    with open(fileName, 'r') as f:
        for line in f:
            parts = line.strip().split('\t')
            genome_name = parts[0]
            taxonomies = parts[1]
            tax_dict = {}
            for tax in taxonomies.split(';'):
                tax_abbr = tax.split('__')[0]
                rank = d[tax_abbr]
                rank_name = tax.split('__')[1]
                tax_dict[rank] = rank_name
            taxonomy_dict[genome_name] = tax_dict


def count_tree(node):
    count = sum([count_tree(c) for c in node['children']])
    node['num_child'] = count
    if len(node['children']) == 0:
        return 1
    return count


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='process GTDB '
                                'taxonomy tsv and GTDB tree to a json format for PhyDo')
    parser.add_argument('--gtdb_tree', help='path to gtdb tree')
    parser.add_argument('--gtdb_taxonomy', help='path to gtdb taxonomy tsv')
    parser.add_argument('--output', help='json output')
    args = parser.parse_args()
    parse_taxonomy(args.gtdb_taxonomy)
    tree = parseNewick(args.gtdb_tree)
    count_tree(tree)
    with open(args.output, 'w', encoding='utf8') as f:
        f.write(json.dumps(tree))
