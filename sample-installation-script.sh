# up to date as of Aug 16, 2019

VERSION=r89

sudo apt-get update && sudo apt-get install -y python3-pip mysql-client  libmysqlclient-dev mysql-server
sudo service mysql start

git clone --depth=1 https://hanchen1996@bitbucket.org/doxeylabcrew/annotree-scripts.git
cd annotree-scripts
sudo pip3 install -r requirements.txt

# download all files
cd ~
mkdir $VERSION && cd $VERSION
mkdir bacteria && mkdir archaea

# download all files (bacteria AND archaea) here using wget, make sure you use screen to avoid interruption
# download everything except for genomics files, and nt_protein files
wget <YOUR FTP URLs>

# in another screen session
# you can sleep then automatically extract all the files
# by the time you come back it should be ready
# it will attempt to move bacteria and archaea to separate folders
VERSION=r89
cd ~/$VERSION &&\
sleep 10800 &&\
mv *_bac_* bacteria &&\
mv *_ar_* archaea &&\
cd bacteria &&\
for name in *.tar.gz; do tar -xvf $name && rm $name; done &&\
cd ../archaea &&\
for name in *.tar.gz; do tar -xvf $name && rm $name; done

# you should have all the data downloaded


# get tigrfam info files
cd ~/$VERSION
mkdir TIGRFAMs_INFO
cd TIGRFAMs_INFO
wget ftp://ftp.jcvi.org/pub/data/TIGRFAMs/TIGRFAMs_15.0_INFO.tar.gz
tar -xzvf TIGRFAMs_15.0_INFO.tar.gz

# additional .tree and taxonomy files for bacteria
# you need .tree, _taxonomy_, and metadata file from https://data.ace.uq.edu.au/public/gtdb/data/releases/
cd ~/$VERSION/bacteria
wget <change the urls> https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/bac120_r89.tree https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/bac120_taxonomy_r89.tsv  https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/bac120_metadata_r89.tsv
cd ~/$VERSION/archaea
wget <change the urls> https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/ar122_metadata_r89.tsv https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/ar122_r89.tree https://data.ace.uq.edu.au/public/gtdb/data/releases/release89/89.0/ar122_taxonomy_r89.tsv

# config file set up
cd ~
mkdir -p ~/output
################################################
### ONLY use the following as a reference, the file name might change in future versions
################################################

cat > ~/bacteria.yaml <<EOF
#####################################################################
##                AnnoTree Database Configuration                  ##
#####################################################################

database:
  database_name: gtdb_bacteria_r89
  host: localhost
  port: 3306
  user: root
  password: root


paths_to_data_files:
  kegg_counts: $HOME/${VERSION}/bacteria/gtdb_${VERSION}_bac_ko_table.tsv
  kegg_tophits_dir: $HOME/${VERSION}/bacteria/ko_top_hit
  metadata: $HOME/${VERSION}/bacteria/bac120_metadata_${VERSION}.tsv
  newick_tree: $HOME/${VERSION}/bacteria/bac120_${VERSION}.tree
  pfam_counts: $HOME/${VERSION}/bacteria/gtdb_${VERSION}_bac_pfam_table.tsv
  pfam_ftp_dir_url: ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/database_files
  pfam_tophits_dir: $HOME/${VERSION}/bacteria/pfam_tophits
  protein_seq_dir: $HOME/${VERSION}/bacteria/protein_files
  gtdb_taxonomy: $HOME/${VERSION}/bacteria/bac120_taxonomy_${VERSION}.tsv
  tigrfam_counts: $HOME/${VERSION}/bacteria/gtdb_${VERSION}_bac_tigrfam_table.tsv
  tigrfam_tophits_dir: $HOME/${VERSION}/bacteria/tigrfam_tophits
  tigrfam_info_dir: $HOME/${VERSION}/TIGRFAMs_INFO


desired_output_paths:
  json_tree: $HOME/output/r89_bac_tree.json
  pfamA_sql: $HOME/output/pfamA_v32.sql
  pfamA_txt: /var/lib/mysql-files/pfamA_v32.txt
  pfam_taxonomy_sql: $HOME/output/taxonomy_v32.sql
  pfam_taxonomy_txt: /var/lib/mysql-files/taxonomy_v32.txt
EOF



# please double check the name, ko_top_hit can be renamed to ko_top_hits

cat > ~/archaea.yaml <<EOF
#####################################################################
##                AnnoTree Database Configuration                  ##
#####################################################################

database:
  database_name: gtdb_archaea_r89
  host: localhost
  port: 3306
  user: root
  password: root


paths_to_data_files:
  kegg_counts: $HOME/${VERSION}/archaea/gtdb_${VERSION}_ar_ko_table.tsv
  kegg_tophits_dir: $HOME/${VERSION}/archaea/ko_top_hits
  metadata: $HOME/${VERSION}/archaea/ar122_metadata_${VERSION}.tsv
  newick_tree: $HOME/${VERSION}/archaea/ar122_${VERSION}.tree
  pfam_counts: $HOME/${VERSION}/archaea/gtdb_${VERSION}_ar_pfam_table.tsv
  pfam_ftp_dir_url: ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/database_files
  pfam_tophits_dir: $HOME/${VERSION}/archaea/pfam_tophits
  protein_seq_dir: $HOME/${VERSION}/archaea/protein_files
  gtdb_taxonomy: $HOME/${VERSION}/archaea/ar122_taxonomy_${VERSION}.tsv
  tigrfam_counts: $HOME/${VERSION}/archaea/gtdb_${VERSION}_ar_tigrfam_table.tsv
  tigrfam_tophits_dir: $HOME/${VERSION}/archaea/tigrfam_tophits
  tigrfam_info_dir: $HOME/${VERSION}/TIGRFAMs_INFO


desired_output_paths:
  json_tree: $HOME/output/${VERSION}_ar_tree.json
  pfamA_sql: $HOME/output/pfamA_v32.sql
  pfamA_txt: /var/lib/mysql-files/pfamA_v32.txt
  pfam_taxonomy_sql: $HOME/output/taxonomy_v32.sql
  pfam_taxonomy_txt: /var/lib/mysql-files/taxonomy_v32.txt
EOF

sudo chmod 777 /var/lib/mysql-files/ # need permission to write to this directory

# begin loading
cd ~/annotree-scripts/gtdb_database/
python3 make_db.py --config $HOME/bacteria.yaml --tracker=./bacteria-tracker.json &&\
python3 make_db.py --config $HOME/archaea.yaml --tracker=./archaea-tracker.json

# change permission back
sudo chmod 700 /var/lib/mysql-files/



