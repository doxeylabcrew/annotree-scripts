#' @title Homoplasy slope ratio
#' 
#' @description 
#' HSR returns the homoplasy slope ratio of binary character states on a 
#' phylogenetic tree
#' 
#' @details
#' The homoplasy slope (HS) is calculated as follows: HS = ES/(t-3) where t = #
#' taxa and ES = extra steps where ES = 1/CI-1 where CI = consistency index.
#' This can be calculated using phangorn::CI. Then HSR = HS(real)/HS(random) 
#' where HS(random) is the average HS of nsims simulated data sets of size t.
#' The probability of either state is equal for all taxa.
#'             
#' @param tree  Phylo object
#' @param data  Named vector of states. Names correspond with tip labels of the
#'              tree
#' @param nsims Number of simulations to run for the estimation of a random
#'              homoplasy slope (default: 100)
#' @return Homoplasy slope ratio of the tip state data and tree
#' @export
#' @author Kerrin Mendler <mendlerke@gmail.com>
#' @references Meier R, Kores P, & Darwin S. 1991. Homoplasy slope ratio: a 
#'             better measurement of observed homoplasy in cladistic analyses. 
#'             Sys. Zool. 40(1):74-88.
HSR <- function(tree, data, nsims=100) {
  require(phangorn)
  
  num_taxa_t <- length(data)
  tip_levels <- as.character(unique(data))
  
  # Homoplasy slope of given data
  data.phyDat <- as.phyDat(sapply(data, as.character), type = "USER", levels = tip_levels)
  es_real <-  1/CI(tree, data.phyDat) - 1
  hs_real <- es_real/(num_taxa_t - 3)
  
  # Average homoplasy slope of random data with same # taxa
  random_data <- replicate(nsims, 
                           setNames(sample(tip_levels, 
                                           num_taxa_t,
                                           replace = TRUE), 
                                    tree$tip.label), 
                           simplify = FALSE)
  random_data.phyDat <- lapply(random_data,
                               as.phyDat, type = "USER", levels = tip_levels)
  es_random <- unlist(lapply(random_data.phyDat, function(tip_states){
    1/CI(tree, tip_states) - 1
  }))
  hs_random <- es_random/(num_taxa_t - 3)
  avg_hs_random <- mean(hs_random)
  
  return(hs_real/avg_hs_random)
}
