#!/bin/bash
database=$1
token_path=${2:-zenodo_token}
if [ ! $database ];then
  echo "Usage: $0 <database> [zenodo_token_path, default ./zenodo_token]"
  >&2 echo 'database is needed'
  exit 1
fi

filename=${database}-db-backup-$(date "+%Y-%m-%d").sql
bash backup-database.sh $database $filename && python upload_zenodo.py --access_token `cat $token_path` $filename.tar.gz
