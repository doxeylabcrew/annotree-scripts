#!/usr/bin/python
import requests
import argparse
from collections import namedtuple
import os
from requests_toolbelt.multipart.encoder import MultipartEncoder, MultipartEncoderMonitor
File = namedtuple('File', ['name'])

def file_size(handle):
    byte = os.fstat(handle.fileno()).st_size
    if byte > 1000000000:
        return '%d G' % (byte // 1000000000)
    elif byte > 1000000:
        return '%d M' % (byte // 1000000)
    else:
        return '%d kB' % (byte // 1000)

base_url = 'https://zenodo.org/api'
class ZenodoApi(object):
    def __init__(self, token):
        self.token = token
    def get(self,url):
        r = requests.get(base_url+url, params={
            'access_token':self.token
            })
        if r.status_code >= 400:
            return (r.status_code, None)
        return (r.status_code, r.json())
    def upload(self, file):
        headers = {"Content-Type": "application/json"}
        r = requests.post(base_url + '/deposit/depositions',
                                params={'access_token': self.token}, json={},
                                headers={"Content-Type": "application/json"})

        print r.status_code
        bucket_url = r.json()['links']['bucket']
        print 'successfull got bucket link %s' % bucket_url
        filename = file.name.split('/')[-1]
        handle = open(file.name, 'rb')
        print 'uploading to Zenodo %s, size %s' % (file.name, file_size(handle))
        r = requests.put('%s/%s' % (bucket_url, filename),
                        data=handle,
                        headers={"Accept":"application/json",
                        "Authorization":"Bearer %s" % self.token,
                        "Content-Type":"application/octet-stream"})
        print r.status_code
        print r.json
        if (r.status_code < 300 and r.status_code >= 200):
            print 'successfully uploaded %s' % file.name
        else:
            raise Exception('File upload failure', r)
        return (r.status_code, r.json)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Uploads specified file to Zenodo')
    parser.add_argument('--access_token',dest='access_token', required=True,
            help='go to https://zenodo.org/account/settings/applications/tokens/new/ to create one')
    parser.add_argument('--files', dest='files', required=True, nargs='+', help='files to upload')
    args = parser.parse_args()
    access_token = args.access_token
    files = args.files
    z = ZenodoApi(access_token)
    for filename in files:
        print 'uploading file %s' % filename
        file = File(filename)
        z.upload(file)
    print 'complete'
