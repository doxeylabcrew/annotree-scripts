#!/bin/bash
database=$1
filename=$2

if [ ! $database ];then
  echo "Usage: $0 <database> [filename]"
  >&2 echo 'database is needed'
  exit 1
fi

if [ ! $filename ];then
  filename=gtdb_bacteria-db-backup-$(date "+%Y-%m-%d").sql
fi
echo "Please enter mysql root password when prompted Enter password"
mysqlpump --skip-definer --routines --triggers --add-drop-table --add-drop-database -u root -p --databases $database > $filename.temp && mv $filename.temp $filename && tar -cvzf $filename.tar.gz $filename && rm $filename
# mysqlpump is a multi-threaded version of mysqldump, it is faster but can be unsafe to backup a database with on-going writing, see https://dba.stackexchange.com/questions/118846/mysqldump-vs-mysqlpump
