#!/bin/python3

import argparse
from Bio import SeqIO
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import os


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    gtdb_id = insert_values['gtdb_id']
    gene_id = insert_values['gene_id']
    sequence = insert_values['sequence']
    query = """INSERT INTO %s (gtdb_id,gene_id,sequence)
                    VALUES('%s','%s','%s')""" % \
            (table, gtdb_id, gene_id, sequence)
    cursor = db.cursor()
    cursor.execute(query)


def load_protein_seqs_db(db, proteins_folder, table):
    for fasta_file in os.listdir(proteins_folder):
        gtdb_id = fasta_file.replace('_protein.faa', '')
        for fasta_record in SeqIO.parse(os.path.join(proteins_folder, fasta_file), 'fasta'):
            insert_data = {
                'gtdb_id': gtdb_id,
                'gene_id': fasta_record.id,
                'sequence': str(fasta_record.seq).replace('*', '')
            }
            insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, protein_seq_dir,
        table_name='protein_sequences'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_protein_seqs_db(db, protein_seq_dir, table=table_name)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load protein sequences into '
                                'protein_sequences table of gtdb_bacteria database')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--table_name', dest='table_name', default='protein_sequences',
                        help='db table name, default protein_sequences', required=False)
    parser.add_argument('--proteins_folder', dest='proteins_folder',
                        help='path to proteins folder containing a `[gtdb_id]_protein.faa` file for every genome', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.proteins_folder, args.table_name)
