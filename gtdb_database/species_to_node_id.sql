INSERT INTO node_species
	(SELECT n.id, get_species(ncbi_taxid) AS species_id FROM node_tax t
		JOIN node n ON t.gtdb_id = n.gtdb_id
		WHERE ncbi_taxid IS NOT NULL);

