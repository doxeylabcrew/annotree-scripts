#!/usr/bin/python3

import argparse
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import os


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    tigrfamId = insert_values['tigrfam_id']
    definition = insert_values['definition'].replace("\'", "\'\'")
    query = """INSERT INTO %s (tigrfam_id,definition)
                    VALUES('%s','%s')""" % (table, tigrfamId, definition)
    cursor = db.cursor()
    cursor.execute(query)


def tigrfam_info_file_to_dict(file_handle):
    '''
    info_file looks like:
    <field>  <entry>
    <field>  <entry> ...

    Entries can also have two spaces in them
    Not all fields are unique to the file. In this case, entries are separated
    by ' & '
    '''
    info_dict = {}
    with open(file_handle, 'r', errors='ignore') as f:
        for line in f.readlines():
            strip_line = line.strip()
            field = strip_line[0:2]
            if field in info_dict:
                info_dict[field] = info_dict[field] + ' & ' + strip_line[4:]
            else:
                info_dict[field] = strip_line[4:]

    return info_dict


def load_tigrfam_definitions(db, info_folder, table='tigrfam_definitions'):
    for info_file in os.listdir(info_folder):

        if not info_file.lower().endswith('.info'):
            continue

        insert_data = {
            'tigrfam_id': '',
            'definition': ''
        }

        tigrfam_info = tigrfam_info_file_to_dict(os.path.join(info_folder, info_file))

        insert_data['tigrfam_id'] = tigrfam_info['AC']

        if 'GS' in tigrfam_info.keys():
            insert_data['definition'] = tigrfam_info['GS'] + '; ' + tigrfam_info['DE']
        else:
            insert_data['definition'] = tigrfam_info['DE']

        insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, info_folder, table='tigrfam_definitions'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_tigrfam_definitions(db, info_folder, table=table)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load TIGRFAM definitions into '
                                            'tigrfam_definitions table of gtdb_bacteria database '
                                            'from a folder downloaded from the JCVI FTP site')
    parser.add_argument('--db_host', dest='db_host', default='localhost', help='MySQL DB host, default localhost',
                        required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root', help='Mysql DB username, default root',
                        required=False)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria', help='db name, default gtdb_bacteria',
                        required=False)
    parser.add_argument('--table_name', dest='table_name', default='tigrfam_definitions',
                        help='db table name, default tigrfam_definitions', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--tigrfam_info_folder', dest='info_folder',
                        help='path to tigrfam info folder containing a file for every tigrfam', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.info_folder, table=args.table_name)
