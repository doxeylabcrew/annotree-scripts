
SET group_concat_max_len = 99999999;

CREATE TABLE db_config_data_files(
    config_param VARCHAR(100) PRIMARY KEY,
    file_name VARCHAR(100),
    file_md5 VARCHAR(100),
    last_updated TIMESTAMP
);

CREATE TABLE gtdb_node(
    gtdb_id VARCHAR(100) PRIMARY KEY,
    node_id INT(11) UNIQUE
);

CREATE TABLE kegg_definitions(
    kegg_id VARCHAR(100) PRIMARY KEY,
    definition VARCHAR(511)
);

CREATE TABLE kegg_node_ids(
    kegg_id VARCHAR(100) PRIMARY KEY,
    node_id_list MEDIUMTEXT
);

CREATE TABLE kegg_top_hits(
    kegg_top_hit_id INT(11),
    gtdb_id VARCHAR(100),
    gene_id VARCHAR(100),
    kegg_id VARCHAR(100),
    eval REAL,
    bitscore DECIMAL(18,1),
    percent_identity DECIMAL(4,1),
    query_percent_alignment DECIMAL(13,10),
    subject_percent_alignment DECIMAL(13,10),
    INDEX(kegg_id, gtdb_id)
);

CREATE TABLE kegg_tree_counts(
    gtdb_id VARCHAR(100),
    kegg_id VARCHAR(100),
    counts BIGINT(21),
    PRIMARY KEY(gtdb_id,kegg_id)
);

CREATE TABLE manual_label(
    id INT(11) PRIMARY KEY,
    manual_rank VARCHAR(255),
    manual_tax_id INT(11),
    curator VARCHAR(255),
    manual_taxon_name VARCHAR(255)
);

CREATE TABLE node(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    level VARCHAR(511),
    gtdb_id VARCHAR(511),
    parent_id INT(11),
    is_leaf TINYINT(1),
    comment VARCHAR(255),
    length DECIMAL(18,5)
);

CREATE TABLE node_gtdb_ranks(
    node_id INT(11),
    rank VARCHAR(100),
    level VARCHAR(100),
    is_highest TINYINT(1),
    PRIMARY KEY(node_id,rank)
);

CREATE TABLE node_species(
    node_id INT(11) PRIMARY KEY,
    species_id INT(11) -- ncbi species id
);

CREATE TABLE node_tax(
    ncbi_taxid INT(10),
    ncbi_taxonomy VARCHAR(511), -- string of taxonomy levels d__Bacteria p__Firmicutes; c__className; ...  
    gtdb_id VARCHAR(100) PRIMARY KEY,
    gtdb_taxonomy VARCHAR(511)
);

CREATE TABLE pfam_node_ids(
    pfam_id VARCHAR(11) PRIMARY KEY,
    node_id_list MEDIUMTEXT
);

CREATE TABLE pfam_top_hits(
    gtdb_id VARCHAR(100),
    gene_id VARCHAR(100),
    pfam_id VARCHAR(11),
    eval REAL,
    bitscore DECIMAL(18,1),
    INDEX (pfam_id, gtdb_id)
);

CREATE TABLE pfam_tree_counts(
    gtdb_id VARCHAR(100),
    pfam_id VARCHAR(11),
    counts BIGINT(21),
    PRIMARY KEY(gtdb_id,pfam_id)
);

CREATE TABLE protein_sequences(
    gene_id VARCHAR(100),
    gtdb_id VARCHAR(100),
    sequence MEDIUMTEXT,
    PRIMARY KEY(gene_id,gtdb_id)
);

CREATE TABLE tigrfam_definitions(
    tigrfam_id VARCHAR(15) PRIMARY KEY,
    definition VARCHAR(511)
);

CREATE TABLE tigrfam_node_ids(
    tigrfam_id VARCHAR(15) PRIMARY KEY,
    node_id_list MEDIUMTEXT
);

CREATE TABLE tigrfam_top_hits(
    gtdb_id VARCHAR(100),
    gene_id VARCHAR(100),
    tigrfam_id VARCHAR(15),
    eval REAL,
    bitscore DECIMAL(18,1),
    INDEX(tigrfam_id, gtdb_id)
);

CREATE TABLE tigrfam_tree_counts(
    gtdb_id VARCHAR(100),
    tigrfam_id VARCHAR(15),
    counts BIGINT(21),
    PRIMARY KEY(gtdb_id, tigrfam_id)
);
