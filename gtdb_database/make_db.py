#!/usr/local/bin/python3

import argparse
import hashlib
import logging
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import os.path
import re
import subprocess
import yaml
import json
# Custom modules
import load_json_to_node_table
import load_gtdb_taxonomy
import load_pfam_tsv
import load_kegg_tsv
import load_tigrfam_tsv
import load_pfam_top_hits
import load_ko_top_hits
import load_tigrfam_top_hits
import load_protein_seqs
import load_kegg_definitions
import load_tigrfam_definitions


def getDb(config_dict, enter_db, bulk_load=0):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    if enter_db:
        conn = MySQLdb.connect(user=config_dict['database']['user'],
                               passwd=config_dict['database']['password'],
                               host=config_dict['database']['host'],
                               port=config_dict['database']['port'],
                               db=config_dict['database']['database_name'],
                               conv=_my_conv,
                               local_infile=bulk_load)
    else:
        conn = MySQLdb.connect(user=config_dict['database']['user'],
                               passwd=config_dict['database']['password'],
                               host=config_dict['database']['host'],
                               port=config_dict['database']['port'],
                               conv=_my_conv,
                               local_infile=bulk_load)
    return conn


def get_mysql_query(config_dict, script):
    query = ('mysql -h ' + config_dict['database']['host'] +
             ' -P ' + str(config_dict['database']['port']) +
             ' -u ' + config_dict['database']['user'] +
             ' -p' + config_dict['database']['password'] +
             ' ' + config_dict['database']['database_name'] +
             ' ' + script)
    return(query)


def initialize_db(config_dict):
    mysql_root_connection = getDb(config_dict, enter_db=False)
    drop_query = 'DROP DATABASE IF EXISTS {};'.format(config_dict['database']['database_name'])
    create_query = 'CREATE DATABASE {};'.format(config_dict['database']['database_name'])
    c = mysql_root_connection.cursor()
    c.execute(drop_query)
    c.execute(create_query)
    mysql_root_connection.commit()


def load_bulk_tables(config_dict):
    load_taxonomy_query = ''.join(['LOAD DATA INFILE \'',
                                   config_dict['desired_output_paths']['pfam_taxonomy_txt'],
                                   '\' INTO TABLE taxonomy;'])
    load_pfamA_query = ''.join(['LOAD DATA INFILE \'',
                                config_dict['desired_output_paths']['pfamA_txt'],
                                '\' INTO TABLE pfamA;'])
    db_connection = getDb(config_dict, enter_db=True, bulk_load=1)
    c = db_connection.cursor()
    c.execute(load_taxonomy_query)
    c.execute(load_pfamA_query)
    db_connection.commit()
    db_connection.close()


def parse_config_yml(config_path):
    return(yaml.load(open(config_path)))


def run_sql_file(sql_path, config_dict):
    db_connection = getDb(config_dict, enter_db=True)
    with open(sql_path, 'r') as sql_handle:
        sql_lines = sql_handle.read()
    # Remove comments
    sql_lines = re.sub('--.*\n', '\n', sql_lines)
    # Split into list of individual commands
    sql_commands = sql_lines.split(';')
    for command in sql_commands:
        if command.strip() != '':
            c = db_connection.cursor()
            c.execute(command)
    db_connection.commit()
    db_connection.close()


def update_config_file_table(config_dict, config_section, config_key):
    db_connection = getDb(config_dict, enter_db=True)
    file_name = config_dict[config_section][config_key]
    file_base_name = os.path.basename(file_name)
    if os.path.isfile(file_name):
        with open(file_name, 'rb') as f:
            file_md5 = hashlib.md5(f.read()).hexdigest()
        insert_query = '''INSERT INTO {0} (config_param, file_name, file_md5)
            VALUES (\'{1}\', \'{2}\', \'{3}\') ON DUPLICATE KEY
            UPDATE file_name=\'{2}\', file_md5=\'{3}\''''.format(
            'db_config_data_files', config_key, file_base_name, file_md5)
    else:
        insert_query = '''INSERT INTO {0} (config_param, file_name)
            VALUES (\'{1}\', \'{2}\') ON DUPLICATE KEY
            UPDATE file_name=\'{2}\''''.format(
            'db_config_data_files', config_key, file_base_name)
    c = db_connection.cursor()
    c.execute(insert_query)
    db_connection.commit()
    db_connection.close()

class TaskTracker(object):
    def __init__(self, logpath):
        self.logpath = logpath
        if not os.path.exists(self.logpath):
            # create new json dump
            with open(self.logpath, 'w') as f:
                json.dump(dict(), f)
        with open(self.logpath, 'r') as f:
            self.m = json.load(f)
        self.f = None

    def is_done(self, name):
        return self.m.get(name, False)

    def mark_done(self, name):
        self.m[name] = True
        with open(self.logpath, 'w') as f:
            return json.dump(self.m, f)

    def close(self):
        if self.f:
            self.f.close()


if __name__ == '__main__':
    logging.basicConfig(
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler('make_db.log'),
            logging.StreamHandler()
        ],
        level=logging.INFO)

    parser = argparse.ArgumentParser(
        description='Create a new AnnoTree database')
    parser.add_argument('--config', dest='config_path',
                        help='Path to configuration file', required=True)
    parser.add_argument('--tracker',
        help='path to where completed tasks are tracked (in json) e.g. ./bacteria-tracker.json;'\
        ' file will be created if not already',
        required=True)
    args = parser.parse_args()
    config_path = args.config_path
    config_dict = parse_config_yml(config_path)
    tracker = TaskTracker(args.tracker)

    if not tracker.is_done('1_json_tree_gen'):
        logging.info('Generating json tree')
        tsv_to_json_query = ('python3 ../tsv-to-json/tsv-to-json.py' +
                           ' --gtdb_tree ' + config_dict['paths_to_data_files']['newick_tree'] +
                           ' --gtdb_taxonomy ' + config_dict['paths_to_data_files']['gtdb_taxonomy'] +
                           ' --output ' + config_dict['desired_output_paths']['json_tree'])
        subprocess.call(tsv_to_json_query, shell=True)
        tracker.mark_done('1_json_tree_gen')

    if not tracker.is_done('2_db_scheme_init'):
        logging.info('Initializing database schema')
        initialize_db(config_dict)
        run_sql_file('create.gtdb_bacteria.sql', config_dict)
        tracker.mark_done('2_db_scheme_init')

    if not tracker.is_done('3_load_json_tree_to_node'):
        logging.info('Loading json tree into node table')
        load_json_to_node_table.run(db_user=config_dict['database']['user'],
                                  db_pw=config_dict['database']['password'],
                                  db_host=config_dict['database']['host'],
                                  db_port=config_dict['database']['port'],
                                  db_name=config_dict['database']['database_name'],
                                  tree_path=config_dict['desired_output_paths']['json_tree'])
        update_config_file_table(config_dict, 'desired_output_paths', 'json_tree')
        tracker.mark_done('3_load_json_tree_to_node')

    if not tracker.is_done('4_load_gtdb_node'):
        logging.info('Loading gtdb_node table')
        run_sql_file('load_gtdb_node.sql', config_dict)
        tracker.mark_done('4_load_gtdb_node')

    if not tracker.is_done('5_load_node_tax'):
        logging.info('Loading GTDB metadata to node_tax table')
        load_gtdb_taxonomy.run(db_user=config_dict['database']['user'],
                               db_pw=config_dict['database']['password'],
                               db_host=config_dict['database']['host'],
                               db_port=config_dict['database']['port'],
                               db_name=config_dict['database']['database_name'],
                               metadata_path=config_dict['paths_to_data_files']['metadata'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'metadata')
        tracker.mark_done('5_load_node_tax')

    if not tracker.is_done('6_load_pfam_tree_counts'):
        logging.info('Loading pfam_tree_counts table')
        load_pfam_tsv.run(db_user=config_dict['database']['user'],
                          db_password=config_dict['database']['password'],
                          db_host=config_dict['database']['host'],
                          db_port=config_dict['database']['port'],
                          db_name=config_dict['database']['database_name'],
                          pfam_tsv_path=config_dict['paths_to_data_files']['pfam_counts'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'pfam_counts')
        tracker.mark_done('6_load_pfam_tree_counts')


    if not tracker.is_done('7_download_pfam_files'):
        logging.info('Getting Pfam\'s files')
        get_pfam_taxonomy_query = ('curl ' + config_dict['paths_to_data_files']['pfam_ftp_dir_url'] +
                                   '/taxonomy.sql.gz | gunzip > ' +
                                   config_dict['desired_output_paths']['pfam_taxonomy_sql'])
        subprocess.call(get_pfam_taxonomy_query, shell=True)
        get_pfam_taxonomy_data_query = ('curl ' + config_dict['paths_to_data_files']['pfam_ftp_dir_url'] +
                                        '/taxonomy.txt.gz | gunzip > ' +
                                        config_dict['desired_output_paths']['pfam_taxonomy_txt'])
        subprocess.call(get_pfam_taxonomy_data_query, shell=True)
        get_pfamA_query = ('curl ' + config_dict['paths_to_data_files']['pfam_ftp_dir_url'] +
                           '/pfamA.sql.gz | gunzip > ' +
                           config_dict['desired_output_paths']['pfamA_sql'])
        subprocess.call(get_pfamA_query, shell=True)
        get_pfamA_data_query = ('curl ' + config_dict['paths_to_data_files']['pfam_ftp_dir_url'] +
                                '/pfamA.txt.gz | gunzip > ' +
                                config_dict['desired_output_paths']['pfamA_txt'])
        subprocess.call(get_pfamA_data_query, shell=True)
        tracker.mark_done('7_download_pfam_files')

    if not tracker.is_done('8_load_pfam_tables'):
        logging.info('Loading Pfam tables')
        run_sql_file(config_dict['desired_output_paths']['pfam_taxonomy_sql'], config_dict)
        update_config_file_table(config_dict, 'desired_output_paths', 'pfam_taxonomy_sql')
        run_sql_file(config_dict['desired_output_paths']['pfamA_sql'], config_dict)
        update_config_file_table(config_dict, 'desired_output_paths', 'pfamA_sql')
        load_bulk_tables(config_dict)  # needs to be tested
        update_config_file_table(config_dict, 'desired_output_paths', 'pfam_taxonomy_txt')
        update_config_file_table(config_dict, 'desired_output_paths', 'pfamA_txt')
        tracker.mark_done('8_load_pfam_tables')


    if not tracker.is_done('9_load_kegg_tree_counts'):
        logging.info('Loading kegg_tree_counts table')
        load_kegg_tsv.run(db_user=config_dict['database']['user'],
                          db_password=config_dict['database']['password'],
                          db_host=config_dict['database']['host'],
                          db_port=config_dict['database']['port'],
                          db_name=config_dict['database']['database_name'],
                          kegg_tsv_path=config_dict['paths_to_data_files']['kegg_counts'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'kegg_counts')
        tracker.mark_done('9_load_kegg_tree_counts')


    if not tracker.is_done('10_load_tigrfam_tree_counts'):
        logging.info('Loading tigrfam_tree_counts table')
        load_tigrfam_tsv.run(db_user=config_dict['database']['user'],
                          db_password=config_dict['database']['password'],
                          db_host=config_dict['database']['host'],
                          db_port=config_dict['database']['port'],
                          db_name=config_dict['database']['database_name'],
                          tigrfam_tsv_path=config_dict['paths_to_data_files']['tigrfam_counts'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'tigrfam_counts')
        tracker.mark_done('10_load_tigrfam_tree_counts')


    if not tracker.is_done('11_defining_mysql_util_func'):
        # TODO Figure out how to parse this one to make it work w/o subprocess.call
        logging.info('Defining MySQL utility functions')
        util_function_query = get_mysql_query(config_dict, '< util_sql_functions.sql')
        subprocess.call(util_function_query, shell=True)

        logging.info('Loading node_species table')
        run_sql_file('species_to_node_id.sql', config_dict)
        tracker.mark_done('11_defining_mysql_util_func')


    if not tracker.is_done('12_load_pfam_top_hits'):
        logging.info('Loading pfam_top_hits table')
        load_pfam_top_hits.run(db_user=config_dict['database']['user'],
                               db_password=config_dict['database']['password'],
                               db_host=config_dict['database']['host'],
                               db_port=config_dict['database']['port'],
                               db_name=config_dict['database']['database_name'],
                               tophits_folder=config_dict['paths_to_data_files']['pfam_tophits_dir'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'pfam_tophits_dir')
        tracker.mark_done('12_load_pfam_top_hits')

    if not tracker.is_done('13_load_kegg_top_hits'):
        logging.info('Loading kegg_top_hits table')
        load_ko_top_hits.run(db_user=config_dict['database']['user'],
                             db_password=config_dict['database']['password'],
                             db_host=config_dict['database']['host'],
                             db_port=config_dict['database']['port'],
                             db_name=config_dict['database']['database_name'],
                             tophits_folder=config_dict['paths_to_data_files']['kegg_tophits_dir'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'kegg_tophits_dir')
        tracker.mark_done('13_load_kegg_top_hits')


    if not tracker.is_done('14_load_tigrfam_top_hits'):
        logging.info('Loading tigrfam_top_hits table')
        load_tigrfam_top_hits.run(db_user=config_dict['database']['user'],
                               db_password=config_dict['database']['password'],
                               db_host=config_dict['database']['host'],
                               db_port=config_dict['database']['port'],
                               db_name=config_dict['database']['database_name'],
                               tophits_folder=config_dict['paths_to_data_files']['tigrfam_tophits_dir'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'tigrfam_tophits_dir')
        tracker.mark_done('14_load_tigrfam_top_hits')

    if not tracker.is_done('15_load_protein_sequnces'):
        logging.info('Loading protein_sequences table')
        load_protein_seqs.run(db_user=config_dict['database']['user'],
                              db_password=config_dict['database']['password'],
                              db_host=config_dict['database']['host'],
                              db_port=config_dict['database']['port'],
                              db_name=config_dict['database']['database_name'],
                              protein_seq_dir=config_dict['paths_to_data_files']['protein_seq_dir'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'protein_seq_dir')
        tracker.mark_done('15_load_protein_sequnces')

    if not tracker.is_done('16_load_kegg_definitions'):
        logging.info('Loading kegg_definitions table')
        load_kegg_definitions.run(db_user=config_dict['database']['user'],
                                  db_password=config_dict['database']['password'],
                                  db_host=config_dict['database']['host'],
                                  db_port=config_dict['database']['port'],
                                  db_name=config_dict['database']['database_name'])
        tracker.mark_done('16_load_kegg_definitions')

    if not tracker.is_done('17_load_tigrfam_definitions'):
        logging.info('Loading tigrfam_definitions table')
        load_tigrfam_definitions.run(db_user=config_dict['database']['user'],
                                  db_password=config_dict['database']['password'],
                                  db_host=config_dict['database']['host'],
                                  db_port=config_dict['database']['port'],
                                  db_name=config_dict['database']['database_name'],
                                  info_folder=config_dict['paths_to_data_files']['tigrfam_info_dir'])
        update_config_file_table(config_dict, 'paths_to_data_files', 'tigrfam_info_dir')
        tracker.mark_done('17_load_tigrfam_definitions')

