#!/usr/bin/python3

import argparse
import csv
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import pandas as pd
import sys

csv.field_size_limit(sys.maxsize)


def getDb(username, password, host, port, database):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def run(db_user, db_pw, db_host, db_port, db_name, metadata_path):
    db = getDb(db_user, db_pw, db_host, db_port, db_name)
    cursor = db.cursor()
    template = """INSERT INTO node_tax (gtdb_id, gtdb_taxonomy, ncbi_taxid, ncbi_taxonomy) VALUES (%s, %s, %s, %s) """
    with open(metadata_path, 'r') as f:
        csvReader = csv.reader(f, delimiter='\t')
        header = next(csvReader)
        # find the column of ncbi_taxonomy
        ncbi_num = header.index('ncbi_taxonomy')
        gtdb_num = header.index('gtdb_taxonomy')
        ncbi_id_num = header.index('ncbi_taxid')
        for line in csvReader:
            gtdb_id = line[0]
            ncbi_taxonomy = line[ncbi_num]
            gtdb_taxonomy = line[gtdb_num]
            ncbi_taxid = line[ncbi_id_num]
            if ncbi_taxid == 'none':
                ncbi_taxid = 0
            if gtdb_taxonomy == 'none':
                gtdb_taxonomy = ''

            query = template % (gtdb_id, gtdb_taxonomy, ncbi_taxid, ncbi_taxonomy)
            cursor.execute(template, [gtdb_id, gtdb_taxonomy, ncbi_taxid, ncbi_taxonomy])
    db.commit()


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='given a GTDB metadata, loads GTDB ')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--gtdb_metadata', help='path to gtdb metadata tsv file', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.metadata)
