#!/usr/bin/python3

import argparse
from Bio.KEGG import REST
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    keggId = insert_values['kegg_id']
    definition = insert_values['definition'].replace("\'", "\'\'")
    query = """INSERT INTO %s (kegg_id,definition)
                    VALUES('%s','%s')""" % (table, keggId, definition)
    cursor = db.cursor()
    cursor.execute(query)


def load_kegg_definitions(db, table='kegg_definitions'):
    kegg_id_definitions = REST.kegg_list("ko").read().rstrip().split('\n')
    for kegg_id_definition in kegg_id_definitions:
        # Sample line: 'ko:K22687\tINHBB; inhibin beta B chain'
        kegg_part, definition_part = kegg_id_definition.split('\t')
        kegg_id = kegg_part.split(':')[1]
        insert_data = {
            'kegg_id': kegg_id,
            'definition': definition_part
        }
        insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, table_name='kegg_definitions'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_kegg_definitions(db, table=table_name)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load KEGG definitions into '
                                            'kegg_definitions table of gtdb_bacteria database '
                                            'using the KEGG REST API')
    parser.add_argument('--db_host', dest='db_host', default='localhost', help='MySQL DB host, default localhost',
                        required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root', help='Mysql DB username, default root',
                        required=False)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria', help='db name, default gtdb_bacteria',
                        required=False)
    parser.add_argument('--table_name', dest='table_name', default='kegg_definitions',
                        help='db table name, default kegg_definitions', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        table=args.table_name)
