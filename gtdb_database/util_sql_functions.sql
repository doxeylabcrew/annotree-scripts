DELIMITER //
DROP FUNCTION IF EXISTS get_species //
CREATE FUNCTION get_species(id int(10) UNSIGNED) RETURNS INT(10) UNSIGNED
BEGIN
DECLARE prev_id int;
DECLARE parent_id int;
DECLARE curr_rank VARCHAR(100);

SET prev_id = id;
SET parent_id = NULL;
SET curr_rank = 'no rank';
SELECT parent,rank INTO parent_id, curr_rank FROM taxonomy WHERE 
ncbi_taxid = prev_id;
WHILE parent_id > 0 AND parent_id != prev_id AND curr_rank != 'species' DO
    SET prev_id = parent_id; 
    SET parent_id = NULL; 
    SELECT parent,rank INTO parent_id, curr_rank FROM taxonomy WHERE 
ncbi_taxid = prev_id;
END WHILE;
RETURN prev_id;
END //
DELIMITER ;

