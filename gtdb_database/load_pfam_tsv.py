#!/usr/bin/python3

import argparse
import csv
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    gtdbId = insert_values['gtdb_id']
    pfamId = insert_values['pfam_id']
    counts = str(insert_values['counts'])
    query = """INSERT INTO %s (gtdb_id,pfam_id,counts)
                    VALUES('%s','%s','%s')""" % (table, gtdbId, pfamId, counts)
    cursor = db.cursor()
    cursor.execute(query)


def load_pfam_db(db, pfam_file, table='pfam_tree_counts'):
    with open(pfam_file, 'r') as tsvfile:
        reader = csv.DictReader(tsvfile, delimiter='\t')
        for row in reader:
            for field in row:
                # skip the ID field and those that have 0 counts
                if field == 'Genome ID' or row[field] == '0':
                    continue
                else:
                    insert_data = {
                        'gtdb_id': row['Genome ID'],
                        'pfam_id': field.split('.')[0],
                        'counts': row[field]
                    }
                    insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, pfam_tsv_path,
        table_name='pfam_tree_counts'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_pfam_db(db, pfam_tsv_path, table=table_name)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load data from GTDB Pfam tsv into '
                                'pfam_tree_counts table of gtdb_bacteria database')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--table_name', dest='table_name', default='pfam_tree_counts',
                        help='db table name, default pfam_tree_counts', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--pfam_tsv', dest='pfam_tsv', help='path to pfam tsv file', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.pfam_tsv, args.table_name)
