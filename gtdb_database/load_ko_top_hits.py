#!/bin/python3

import argparse
import csv
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import os


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    kegg_top_hit_id = insert_values['kegg_top_hit_id']
    gtdb_id = insert_values['gtdb_id']
    gene_id = insert_values['gene_id']
    ko_id = insert_values['ko_id']
    eval = insert_values['eval']
    bitscore = insert_values['bitscore']
    percent_identity = insert_values['percent_identity']
    query_percent_alignment = insert_values['query_percent_alignment']
    subject_percent_alignment = insert_values['subject_percent_alignment']

    query = """INSERT INTO %s (kegg_top_hit_id, gtdb_id,gene_id,kegg_id,eval,bitscore,percent_identity,query_percent_alignment,subject_percent_alignment)
                    VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s')""" % \
            (table, kegg_top_hit_id, gtdb_id, gene_id, ko_id, eval, bitscore,
             percent_identity, query_percent_alignment, subject_percent_alignment)
    cursor = db.cursor()
    cursor.execute(query)


def load_kegg_hits_db(db, tophits_folder, table, id_start):
    kegg_top_hit_id = id_start - 1
    for genome_file in os.listdir(tophits_folder):
        gtdb_id = genome_file.replace('_ko_hits.tsv', '')
        with open(os.path.join(tophits_folder, genome_file), 'r') as genome_file_full_path:
            reader = csv.reader(genome_file_full_path, delimiter='\t')
            header = next(reader)
            gene_id_index = header.index('Gene Id')
            tophits_index = header.index(
                'Top hits (KO id,e-value,bitscore,percent identity,query percent alignment,subject percent alignment)')
            for line in reader:
                lo_kegg_hits = line[tophits_index].split(';')
                for kegg_hit in lo_kegg_hits:
                    kegg_top_hit_id += 1
                    hit_data = kegg_hit.split(',')
                    kegg_terms = hit_data[0].split('&')
                    for kegg_term in kegg_terms:
                        insert_data = {
                            'kegg_top_hit_id': kegg_top_hit_id,
                            'gtdb_id': gtdb_id,
                            'gene_id': line[gene_id_index],
                            'ko_id': kegg_term,
                            'eval': hit_data[1],
                            'bitscore': hit_data[2],
                            'percent_identity': hit_data[3],
                            'query_percent_alignment': hit_data[4],
                            'subject_percent_alignment': hit_data[5]
                        }
                        insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, tophits_folder,
        table_name='kegg_top_hits'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_kegg_hits_db(db, tophits_folder, table=table_name, id_start=1)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load data from ko_tophits folder into '
                                'kegg_top_hits table of gtdb_bacteria database')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--table_name', dest='table_name', default='kegg_top_hits',
                        help='db table name, default kegg_top_hits', required=False)
    parser.add_argument('--tophits_folder', dest='tophits_folder',
                        help='path to ko tophits folder containing a file for every genome', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.tophits_folder, args.table_name)
