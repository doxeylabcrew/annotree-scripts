#!/usr/bin/python3

import argparse
import json
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions


def quote(s):
    return "'%s'" % s


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, parent_id=None, table=None):
    node_id = insert_values['id'] or 'NULL'
    level = ("'" + insert_values['level'] + "'") if insert_values['level'] else 'NULL'
    length = str(insert_values['length']) if insert_values['length'] else 'NULL'
    parent_id = parent_id or 'NULL'
    if insert_values['isLeaf'] > 0:
        isLeaf = '1'
        gtdb_id = level
    else:
        isLeaf = '0'
        gtdb_id = 'NULL'
    query = """INSERT INTO %s (id, level, gtdb_id, parent_id,is_leaf,length)
                    VALUES(%s,%s,%s,%s,%s,%s)""" % \
        (table, node_id, level, gtdb_id, parent_id, isLeaf, length)
    cursor = db.cursor()
    cursor.execute(query)
    rowid = cursor.lastrowid

    ranks = set([r['rank'] for r in insert_values['ranks']])
    taxonomies = ['domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    highest_rank = None
    for t in taxonomies:
        if t in ranks:
            highest_rank = t
            break
    if highest_rank is None and len(ranks) > 0:
        print(ranks)
        print(insert_values['id'])
        raise Exception('cannot find highest rank after searching through domain to species')
    # as a side effect, populate node_gtdb_ranks
    for rank in insert_values['ranks']:

        query = """INSERT INTO node_gtdb_ranks (node_id, rank, level, is_highest)
                        VALUES(%s,%s,%s,%s)""" % \
            (rowid, quote(rank['rank']), quote(rank['rank_name']),
             '1' if rank['rank'] == highest_rank else '0')
        cursor = db.cursor()
        cursor.execute(query)
    return rowid


def _load_tree_db(db, node_dict, parent_id=None, table=None):
    parent_id = insert_row(db, node_dict, parent_id=parent_id, table=table)
    for child in node_dict['children']:
        _load_tree_db(db, child, parent_id=parent_id, table=table)


def load_tree_db(db, jsonpath, table='node'):
    f = open(jsonpath, 'r')
    node_dict = json.load(f)
    f.close()
    _load_tree_db(db, node_dict, table=table)
    db.commit()


def run(db_user, db_pw, db_host, db_port, db_name, tree_path):
    db = getDb(username=db_user, password=db_pw, host=db_host, port=db_port,
               database=db_name)
    load_tree_db(db, tree_path, table='node')


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load data from GTDB tree into '
                                'node table and node_gtdb_ranks table of gtdb_bacteria database')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--gtdb_tree', help='path to gtdb tree json file', required=True)
    args = parser.parse_args()
    run(db_user=args.db_user, db_pw=args.db_password, db_host=args.db_host,
        db_port=args.db_port, db_name=args.db_name, tree_path=args.gtdb_tree)
