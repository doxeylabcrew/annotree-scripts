#!/bin/python3

import argparse
import csv
import MySQLdb
from MySQLdb.constants import FIELD_TYPE
from MySQLdb.converters import conversions
import os


def getDb(username, password, host, port, database='gtdb_bacteria'):
    _my_conv = conversions.copy()
    _my_conv.update({FIELD_TYPE.LONG: int, FIELD_TYPE.INT24: int})
    conn = MySQLdb.connect(user=username, passwd=password, host=host,
                           port=port, db=database, conv=_my_conv)
    return conn


def insert_row(db, insert_values, table=None):
    gtdb_id = insert_values['gtdb_id']
    gene_id = insert_values['gene_id']
    tigrfam_id = insert_values['tigrfam_id']
    eval = insert_values['eval']
    bitscore = insert_values['bitscore']
    query = """INSERT INTO %s (gtdb_id,gene_id,tigrfam_id,eval,bitscore)
                    VALUES('%s','%s','%s','%s','%s')""" % \
            (table, gtdb_id, gene_id, tigrfam_id, eval, bitscore)
    cursor = db.cursor()
    cursor.execute(query)


def load_tigrfam_hits_db(db, tophits_folder, table):
    for genome_file in os.listdir(tophits_folder):
        gtdb_id = genome_file.replace('_tigrfam_tophit.tsv', '')
        with open(os.path.join(tophits_folder, genome_file), 'r') as genome_file_full_path:
            reader = csv.reader(genome_file_full_path, delimiter='\t')
            header = next(reader)
            gene_id_index = header.index('Gene Id')
            tophits_index = header.index('Top hits (Family id,e-value,bitscore)')
            for line in reader:
                lo_tigrfam_hits = line[tophits_index].split(';')
                for tigrfam_hit in lo_tigrfam_hits:
                    hit_data = tigrfam_hit.split(',')
                    insert_data = {
                        'gtdb_id': gtdb_id,
                        'gene_id': line[gene_id_index],
                        'tigrfam_id': hit_data[0].split('.')[0],
                        'eval': hit_data[1],
                        'bitscore': hit_data[2]
                    }
                    insert_row(db, insert_data, table)
    db.commit()


def run(db_user, db_password, db_host, db_port, db_name, tophits_folder,
        table_name='tigrfam_top_hits'):
    db = getDb(db_user, db_password, db_host, db_port, db_name)
    load_tigrfam_hits_db(db, tophits_folder, table=table_name)


if __name__ == '__main__':
    parser = \
        argparse.ArgumentParser(description='load data from tigrfam hit tsv into '
                                'tigrfam_top_hits table of gtdb_bacteria database')
    parser.add_argument('--db_host', dest='db_host', default='localhost',
                        help='MySQL DB host, default localhost', required=False)
    parser.add_argument('--db_port', dest='db_port', default=3306,
                        help='MySQL DB port, default 3306', required=False)
    parser.add_argument('--db_user', dest='db_user', default='root',
                        help='Mysql DB username, default root', required=False)
    parser.add_argument('--db_password', dest='db_password',
                        help='Mysql DB password', required=True)
    parser.add_argument('--db_name', dest='db_name', default='gtdb_bacteria',
                        help='db name, default gtdb_bacteria', required=False)
    parser.add_argument('--table_name', dest='table_name', default='tigrfam_top_hits',
                        help='db table name, default tigrfam_top_hits', required=False)
    parser.add_argument('--tophits_folder', dest='tophits_folder',
                        help='path to tigrfam tophits folder containing a file for every genome', required=True)
    args = parser.parse_args()
    run(args.db_user, args.db_password, args.db_host, args.db_port, args.db_name,
        args.tophits_folder, args.table_name)
